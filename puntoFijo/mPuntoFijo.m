function varargout = mPuntoFijo(varargin)
% MPUNTOFIJO MATLAB code for mPuntoFijo.fig
%      MPUNTOFIJO, by itself, creates a new MPUNTOFIJO or raises the existing
%      singleton*.
%
%      H = MPUNTOFIJO returns the handle to a new MPUNTOFIJO or the handle to
%      the existing singleton*.
%
%      MPUNTOFIJO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MPUNTOFIJO.M with the given input arguments.
%
%      MPUNTOFIJO('Property','Value',...) creates a new MPUNTOFIJO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mPuntoFijo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mPuntoFijo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mPuntoFijo

% Last Modified by GUIDE v2.5 06-Apr-2019 18:05:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mPuntoFijo_OpeningFcn, ...
                   'gui_OutputFcn',  @mPuntoFijo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mPuntoFijo is made visible.
function mPuntoFijo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mPuntoFijo (see VARARGIN)

% Choose default command line output for mPuntoFijo
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mPuntoFijo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mPuntoFijo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function cifras_Callback(hObject, eventdata, handles)
% hObject    handle to cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cifras as text
%        str2double(get(hObject,'String')) returns contents of cifras as a double


% --- Executes during object creation, after setting all properties.
function cifras_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function valorIn_Callback(hObject, eventdata, handles)
% hObject    handle to valorIn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of valorIn as text
%        str2double(get(hObject,'String')) returns contents of valorIn as a double


% --- Executes during object creation, after setting all properties.
function valorIn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to valorIn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Calcular.
function Calcular_Callback(hObject, eventdata, handles)
% hObject    handle to Calcular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc
format long
syms x
fd = eval(get(handles.fd,'String'));
%f=eval(get(handles.funcion,'String'));
cifras=str2double(get(handles.cifras,'String'));
error =(0.5*10^(2-cifras));
iter=0;
format long
xo=str2double(get(handles.valorIn,'String'));


dfd = diff(fd);
edfd = subs(dfd,xo);
abs(edfd);

disp(edfd);

if(edfd<1)
  errorA1=10000000;
  fprintf('Iteracion \t\t Xn \t\t\t Xn+1 \t\t\t\t error \n')
    while(errorA1>error)
        iter=iter+1;
        anterior=xo;
        xo=subs(fd,anterior);
        errorA1=(abs((xo-anterior)/xo))*100;
        fprintf('%3.0f \t %2.15f \t %2.15f \t %e\n', iter, anterior,xo,errorA1);
    end
        fprintf('El valor aproximado de x es: %2.15f\n', xo)
        fprintf('El error es: %e\n',errorA1);
        set(handles.raiz,'String', double(xo));
        set(handles.tolerancia,'String', error);
        set(handles.error,'String', double(errorA1));
        set(handles.iteraciones,'String', iter);
else
    disp('la funci�n no converge');
end
% set(handles.raiz,'String',xo);
% set(handles.error,'String',errorA1);
% set(handles.tolerancia,'String',error);
% set(handles.iteraciones,'String',iter);

function raiz_Callback(hObject, eventdata, handles)
% hObject    handle to raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of raiz as text
%        str2double(get(hObject,'String')) returns contents of raiz as a double


% --- Executes during object creation, after setting all properties.
function raiz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tolerancia_Callback(hObject, eventdata, handles)
% hObject    handle to tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tolerancia as text
%        str2double(get(hObject,'String')) returns contents of tolerancia as a double


% --- Executes during object creation, after setting all properties.
function tolerancia_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function error_Callback(hObject, eventdata, handles)
% hObject    handle to error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of error as text
%        str2double(get(hObject,'String')) returns contents of error as a double


% --- Executes during object creation, after setting all properties.
function error_CreateFcn(hObject, eventdata, handles)
% hObject    handle to error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function funcion_Callback(hObject, eventdata, handles)
% hObject    handle to funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of funcion as text
%        str2double(get(hObject,'String')) returns contents of funcion as a double


% --- Executes during object creation, after setting all properties.
function funcion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function iteraciones_Callback(hObject, eventdata, handles)
% hObject    handle to iteraciones (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of iteraciones as text
%        str2double(get(hObject,'String')) returns contents of iteraciones as a double


% --- Executes during object creation, after setting all properties.
function iteraciones_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iteraciones (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in graficar.
function graficar_Callback(hObject, eventdata, handles)
% hObject    handle to graficar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 gra=get(handles.funcion,'String');
 syms x;
 ezplot(handles.axes1,gra);
 grid on
 hold on
    



% --- Executes on button press in limpiar.
function limpiar_Callback(hObject, eventdata, handles)
% hObject    handle to limpiar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 set(handles.raiz,'String', '');
    set(handles.tolerancia,'String', '');
    set(handles.error,'String', '');
    set(handles.funcion,'String', '');
    set(handles.valorIn,'String', '');
    set(handles.fd,'String', '');
    set(handles.cifras,'String', '');
    set(handles.iteraciones,'String','');
    cla reset;



function fd_Callback(hObject, eventdata, handles)
% hObject    handle to fd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fd as text
%        str2double(get(hObject,'String')) returns contents of fd as a double


% --- Executes during object creation, after setting all properties.
function fd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
