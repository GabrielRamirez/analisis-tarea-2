function varargout = newtonRaphson(varargin)
% NEWTONRAPHSON MATLAB code for newtonRaphson.fig
%      NEWTONRAPHSON, by itself, creates a new NEWTONRAPHSON or raises the existing
%      singleton*.
%
%      H = NEWTONRAPHSON returns the handle to a new NEWTONRAPHSON or the handle to
%      the existing singleton*.
%
%      NEWTONRAPHSON('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NEWTONRAPHSON.M with the given input arguments.
%
%      NEWTONRAPHSON('Property','Value',...) creates a new NEWTONRAPHSON or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before newtonRaphson_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to newtonRaphson_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help newtonRaphson

% Last Modified by GUIDE v2.5 06-Apr-2019 18:02:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @newtonRaphson_OpeningFcn, ...
                   'gui_OutputFcn',  @newtonRaphson_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before newtonRaphson is made visible.
function newtonRaphson_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to newtonRaphson (see VARARGIN)

% Choose default command line output for newtonRaphson
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes newtonRaphson wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = newtonRaphson_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txt_funcion_Callback(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_funcion as text
%        str2double(get(hObject,'String')) returns contents of txt_funcion as a double


% --- Executes during object creation, after setting all properties.
function txt_funcion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_graficar.
function btn_graficar_Callback(hObject, eventdata, handles)
    f=get(handles.txt_funcion,'string');
    syms x;
    ezplot(handles.axes1,f);
    grid on
    hold on
    disp('Fin graficador'); 

function txt_valor_inicial_Callback(hObject, eventdata, handles)
% hObject    handle to txt_valor_inicial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_valor_inicial as text
%        str2double(get(hObject,'String')) returns contents of txt_valor_inicial as a double


% --- Executes during object creation, after setting all properties.
function txt_valor_inicial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_valor_inicial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_cifras_Callback(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_cifras as text
%        str2double(get(hObject,'String')) returns contents of txt_cifras as a double


% --- Executes during object creation, after setting all properties.
function txt_cifras_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_calcular.
function btn_calcular_Callback(hObject, eventdata, handles)
clc
format long
syms x
disp('Metodo de Newton Raphson')

%Sección de introducción de datos de trabajo
% Para la funcion de trabajo, esta debe estar en el formato f(x)=0
f = eval(get(handles.txt_funcion, 'String'));
x0 = str2double(get(handles.txt_valor_inicial, 'String'));
cifras = str2double(get(handles.txt_cifras, 'String'));
error =(0.5*10^(2-cifras));
set(handles.txt_tolerancia,'String', error);

w = subs(f,x0);
G = diff(f);
f1 = subs(f,x0);
f2 = subs(G,x0);
h = diff(G);
f1d = subs(h,x0);
% disp(double(f1));
% disp(double(f2));
% disp(double(f1d));
cv=abs(f1*f1d/(f2)^2);
%disp(double(cv));

if(cv<1)
P = x0 - f1/f2;
cont = 1;
tol = abs(P - x0);
ea = (P-x0)/P;
ea = abs(ea);
ea = ea*100; 
% Mostrar los valores solicitados en pantalla
fprintf('Iteracion \t\t Xn \t\t\t Xn+1 \t\t\t\t error \n')
fprintf('%3.0f \t %2.15f \t %2.15f \t %e\n', cont, x0,P,ea)


while(ea > error)
	cont = cont + 1;
	x0 = P;
	f1 = subs(f,x0);
	f2 = subs(G,x0);
	P = x0 - f1/f2;
	tol = abs(P - x0);
    ea = (P-x0)/P;
    ea = abs(ea);
    ea = ea*100;  
    
	
	fprintf('%3.0f \t %2.15f \t %2.15f \t %e\n', cont, x0,P,ea)
end

% Mostrar respuesta aproximada en pantalla
fprintf('\n')
fprintf('El valor aproximado de x es: %2.15f\n', P)
fprintf('El error es: %e\n',ea);
set(handles.txt_raiz,'String', double(P));
set(handles.txt_error,'String', double(ea));

else
    fprintf('La funcion no converge');
end



function txt_raiz_Callback(hObject, eventdata, handles)
% hObject    handle to txt_raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_raiz as text
%        str2double(get(hObject,'String')) returns contents of txt_raiz as a double


% --- Executes during object creation, after setting all properties.
function txt_raiz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_error_Callback(hObject, eventdata, handles)
% hObject    handle to txt_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_error as text
%        str2double(get(hObject,'String')) returns contents of txt_error as a double


% --- Executes during object creation, after setting all properties.
function txt_error_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_tolerancia_Callback(hObject, eventdata, handles)
% hObject    handle to txt_tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_tolerancia as text
%        str2double(get(hObject,'String')) returns contents of txt_tolerancia as a double


% --- Executes during object creation, after setting all properties.
function txt_tolerancia_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_limpiar.
function btn_limpiar_Callback(hObject, eventdata, handles)
    set(handles.txt_raiz,'String', '');
    set(handles.txt_tolerancia,'String', '');
    set(handles.txt_error,'String', '');
    set(handles.txt_funcion,'String', '');
    set(handles.txt_valor_inicial,'String', '');
    set(handles.txt_cifras,'String', '');
    cla reset;
