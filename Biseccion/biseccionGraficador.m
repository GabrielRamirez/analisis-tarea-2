function varargout = biseccionGraficador(varargin)
% BISECCIONGRAFICADOR MATLAB code for biseccionGraficador.fig
%      BISECCIONGRAFICADOR, by itself, creates a new BISECCIONGRAFICADOR or raises the existing
%      singleton*.
%
%      H = BISECCIONGRAFICADOR returns the handle to a new BISECCIONGRAFICADOR or the handle to
%      the existing singleton*.
%
%      BISECCIONGRAFICADOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BISECCIONGRAFICADOR.M with the given input arguments.
%
%      BISECCIONGRAFICADOR('Property','Value',...) creates a new BISECCIONGRAFICADOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before biseccionGraficador_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to biseccionGraficador_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help biseccionGraficador

% Last Modified by GUIDE v2.5 02-Apr-2019 11:18:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @biseccionGraficador_OpeningFcn, ...
                   'gui_OutputFcn',  @biseccionGraficador_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before biseccionGraficador is made visible.
function biseccionGraficador_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to biseccionGraficador (see VARARGIN)

% Choose default command line output for biseccionGraficador
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes biseccionGraficador wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = biseccionGraficador_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in graficar.
function graficar_Callback(hObject, eventdata, handles)
    f=get(handles.fun,'string');
    syms x;
    ezplot(handles.axes1,f);
    grid on
    hold on
    disp('Fin graficador'); 

function fun_Callback(hObject, eventdata, handles)
% hObject    handle to fun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fun as text
%        str2double(get(hObject,'String')) returns contents of fun as a double


% --- Executes during object creation, after setting all properties.
function fun_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_limite_inferior_Callback(hObject, eventdata, handles)
% hObject    handle to txt_limite_inferior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_limite_inferior as text
%        str2double(get(hObject,'String')) returns contents of txt_limite_inferior as a double


% --- Executes during object creation, after setting all properties.
function txt_limite_inferior_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_limite_inferior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_limite_superior_Callback(hObject, eventdata, handles)
% hObject    handle to txt_limite_superior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_limite_superior as text

%        str2double(get(hObject,'String')) returns contents of txt_limite_superior as a double


% --- Executes during object creation, after setting all properties.
function txt_limite_superior_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_limite_superior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_calcular.
function btn_calcular_Callback(hObject, eventdata, handles)

clc
format long
syms x
disp('Metodo de Biseccion')

% Seccion de introduccion de datos de trabajo
% Para la funcion de trabajo, esta debe estar en el formato f(x)=0
x1 = str2double(get(handles.txt_limite_inferior, 'String'));
x2 = str2double(get(handles.txt_limite_superior, 'String'));

syms x f
f = eval(get(handles.fun, 'String'));

%g = 2*sin(x)-x*[1+cos(x)];
cifras = str2double(get(handles.txt_cifras, 'String'));
error =(0.5*10^(2-cifras));
set(handles.txt_tolerancia,'String', error);
fx1 = subs(f,x1);
fx2 = subs(f,x2);


if(fx1*fx2 < 0)
	xr = x1 + (x2 - x1)/2;
	fc = subs(f,xr);
	cont = 1;
    ea ="";
	fprintf('Iteracion \t\t X1 \t\t\t\t X2 \t\t\t\t\t Xr \t\t\t\t fx1 \t\t\t\t fx2 \t\t\t\t fxr \t\t\t  error \n')
	fprintf('%3.0f \t %2.15f \t %2.15f \t %2.15f\t %2.15f\t %2.15f\t %2.15f \t %e\n',cont,x1,x2,xr,fx1,fx2,fc,ea);

	%2a verificacion: El programa debe seguir corriendo mientras no se cumpla el criterio de paro
	% Dentro del while, debera verificarse bajo que limite es que se encuentra el cambio de signo
	% y realizar el cambio para continuar evaluando
    ea = 10000000000;
	while(ea > error)
         
		cont = cont + 1;

        if(fx1*fc < 0)
			%a = a;
			x2 = xr;
            fx2 = subs(f,x2);
			xr = x1 + (x2 - x1)/2;
            ea=(abs((xr - x2)/xr))*100;
			%tol = abs(c - b);
            ea=(xr - x2)/xr;
            ea = abs(ea);
            ea = ea*100;
        end
		if (fx1*fc > 0)
			x1 = xr;
			%b = b;
            fx1 = subs(f,x1);
			xr = x1 + (x2 - x1)/2;
            %ea=(abs((c - b)/c))*100;
			%tol = abs(c - a);
            ea=(xr - x1)/xr;
            ea = abs(ea);
            ea = ea*100;
		end

		fc = subs(f,xr);
		fprintf('%3.0f \t %2.15f \t %2.15f \t %2.15f\t %2.15f\t %2.15f\t %2.15f \t %e\n',cont,x1,x2,xr,fx1,fx2,fc,ea);
        
	end

	% Mostrar respuesta aproximada en pantalla
	fprintf('\n');
	fprintf('El valor de la raiz es: %2.15f\n', xr);
    fprintf('El error es: %e\n',ea);
    set(handles.txt_raiz,'String', xr);
    set(handles.txt_error,'String', ea);
else 
   fprintf('La funcion no converge en el intervalo\n'); 
end

function txt_cifras_Callback(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_cifras as text
%        str2double(get(hObject,'String')) returns contents of txt_cifras as a double


% --- Executes during object creation, after setting all properties.
function txt_cifras_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_raiz_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function txt_raiz_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_tolerancia_Callback(hObject, eventdata, handles)

function txt_tolerancia_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_error_Callback(hObject, eventdata, handles)

function txt_error_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_limpiar.
function btn_limpiar_Callback(hObject, eventdata, handles)

    set(handles.txt_raiz,'String', '');
    set(handles.txt_tolerancia,'String', '');
    set(handles.txt_error,'String', '');
    set(handles.fun,'String', '');
    set(handles.txt_limite_inferior,'String', '');
    set(handles.txt_limite_superior,'String', '');
    set(handles.txt_cifras,'String', '');
    cla reset;
