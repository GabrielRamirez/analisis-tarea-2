function varargout = falsa_posicion(varargin)
% FALSA_POSICION MATLAB code for falsa_posicion.fig
%      FALSA_POSICION, by itself, creates a new FALSA_POSICION or raises the existing
%      singleton*.
%
%      H = FALSA_POSICION returns the handle to a new FALSA_POSICION or the handle to
%      the existing singleton*.
%
%      FALSA_POSICION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FALSA_POSICION.M with the given input arguments.
%
%      FALSA_POSICION('Property','Value',...) creates a new FALSA_POSICION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before falsa_posicion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to falsa_posicion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help falsa_posicion

% Last Modified by GUIDE v2.5 06-Apr-2019 17:13:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @falsa_posicion_OpeningFcn, ...
                   'gui_OutputFcn',  @falsa_posicion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before falsa_posicion is made visible.
function falsa_posicion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to falsa_posicion (see VARARGIN)

% Choose default command line output for falsa_posicion
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes falsa_posicion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = falsa_posicion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txt_funcion_Callback(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_funcion as text
%        str2double(get(hObject,'String')) returns contents of txt_funcion as a double


% --- Executes during object creation, after setting all properties.
function txt_funcion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_graficar.
function btn_graficar_Callback(hObject, eventdata, handles)
    f=get(handles.txt_funcion,'string');
    syms x;
    ezplot(handles.axes1,f);
    grid on
    hold on
    disp('Fin graficador'); 



function txt_limite_superior_Callback(hObject, eventdata, handles)
% hObject    handle to txt_limite_superior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_limite_superior as text
%        str2double(get(hObject,'String')) returns contents of txt_limite_superior as a double


% --- Executes during object creation, after setting all properties.
function txt_limite_superior_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_limite_superior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_limite_inferior_Callback(hObject, eventdata, handles)
% hObject    handle to txt_limite_inferior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_limite_inferior as text
%        str2double(get(hObject,'String')) returns contents of txt_limite_inferior as a double


% --- Executes during object creation, after setting all properties.
function txt_limite_inferior_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_limite_inferior (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_cifras_Callback(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_cifras as text
%        str2double(get(hObject,'String')) returns contents of txt_cifras as a double


% --- Executes during object creation, after setting all properties.
function txt_cifras_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_cifras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_calcular.
function btn_calcular_Callback(hObject, eventdata, handles)
clc
format short
disp('Metodo del Punto Fijo')

x1 = str2double(get(handles.txt_limite_inferior, 'String'));
x2 = str2double(get(handles.txt_limite_superior, 'String'));

syms x g
g = eval(get(handles.txt_funcion, 'String'));


cifras = str2double(get(handles.txt_cifras, 'String'));
error =(0.5*10^(2-cifras));
set(handles.txt_tolerancia,'String', double(error));


fx1 = subs(g,x1);
fx2 = subs(g,x2);

if(fx1*fx2<0)
	xr = x2 - fx2*(x1-x2)/(fx1 - fx2);
	fxr = subs(g,xr);
	cont = 1;
	tol = abs(xr - x2);
    ea="";

	fprintf('Iteracion \t\t\t x1 \t\t\t x2\t\t\t f(x1)\t\t\t\t\t f(x2)\t\t\t\t\t f(xr)\t\t\t xr \t\t\t EA \n')
	fprintf('%3.0f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.15f\n',cont,x1,x2,fx1,fx2,fxr,xr,ea);
    ea=1000000000000;
	while(ea > error)
    
	cont = cont + 1;

    if(fx1*fxr < 0)
		
		x2 = xr;
        fx2=subs(g,x2);
		xr = x2 - fx2*(x1-x2)/(fx1 - fx2);
        ea=(xr - x2)/xr;
        ea = abs(ea);
        ea = ea*100; 
        else
		x1 = xr;
		
        fx1=subs(g,double(x1));
     
		xr = x2 - fx2*(x1-x2)/(fx1 - fx2);
	    ea=(xr - x1)/xr;
        ea = abs(ea);
        ea = ea*100;

    end

	fxr = subs(g,double(xr));	
    fprintf('%3.0f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.15f \t %2.17f\n',cont,x1,x2,fx1,fx2,fxr,xr,ea);
    
	end

	fprintf('\n');
	fprintf('El valor aproximado de xr es: %2.15f\n', xr);
    fprintf('El error es: %2.15f\n', ea);
     
    set(handles.txt_raiz,'String', double(xr));
    
    set(handles.txt_error,'String', double(ea));
    
else
    fprintf(' La funcion no converge en el intervalo dado\n');
    
	
end



function txt_raiz_Callback(hObject, eventdata, handles)
% hObject    handle to txt_raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_raiz as text
%        str2double(get(hObject,'String')) returns contents of txt_raiz as a double


% --- Executes during object creation, after setting all properties.
function txt_raiz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_raiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_error_Callback(hObject, eventdata, handles)
% hObject    handle to txt_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_error as text
%        str2double(get(hObject,'String')) returns contents of txt_error as a double


% --- Executes during object creation, after setting all properties.
function txt_error_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_tolerancia_Callback(hObject, eventdata, handles)
% hObject    handle to txt_tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_tolerancia as text
%        str2double(get(hObject,'String')) returns contents of txt_tolerancia as a double


% --- Executes during object creation, after setting all properties.
function txt_tolerancia_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_tolerancia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_limpiar.
function btn_limpiar_Callback(hObject, eventdata, handles)
    set(handles.txt_raiz,'String', '');
    set(handles.txt_tolerancia,'String', '');
    set(handles.txt_error,'String', '');
    set(handles.txt_funcion,'String', '');
    set(handles.txt_limite_inferior,'String', '');
    set(handles.txt_limite_superior,'String', '');
    set(handles.txt_cifras,'String', '');
    cla;
